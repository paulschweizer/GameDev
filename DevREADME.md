### Google Drive
Please find the accompanying google docs here:
[Google Drive Folder](https://drive.google.com/folderview?id=0B_5pqWUPN6WhdThUS2d1bnNRRTg&usp=sharing)

### How to Setup
1. Create a new and empty Unity project
2. Clone this repo into the __Assets__ Folder of the Project
3. Add this [folder](https://www.dropbox.com/sh/6ath0ahj7swofpy/AADVTL_DcPJjjubZOKzhQBDAa?dl=0) to your Dropbox and symlink it into the Unity Project's __/Assets__ folder.

### Code Planning
This is a mind map document and not a definite plan
![Code Planning](/ProjectManagement/CodePlanning.jpg?raw=true "Code Planning")
