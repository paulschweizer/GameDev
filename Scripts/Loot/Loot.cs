using UnityEngine;

/// <summary>
/// A Loot object that can be picked up by the Player.</summary>
public class Loot : MonoBehaviour
{
    /// <summary>
    /// Items in the Loot.</summary>
    public Inventory _inventory;

    /// <summary>
    /// Collect the Items on collision.
    /// Destroy the Loot Object.</summary>
    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<Player>()._inventory.AddInventory(_inventory);
            Destroy(gameObject);
        }
    }
}
