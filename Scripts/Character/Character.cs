﻿using UnityEngine;
using System;
using System.Collections.Generic;

/// <summary>
/// Basic Character Class.</summary>
[Serializable]
[RequireComponent (typeof (NavMeshAgent))]
[RequireComponent (typeof (Rigidbody))]
[RequireComponent (typeof (Inventory))]
public class Character : MonoBehaviour, IStateEvents
{
    /// <summary>
    /// The Inventory of this Character.</summary>
    [HideInInspector] public Inventory _inventory;

    /// <summary>
    /// The Stats of this Character.</summary>
    public Stats _stats;

    /// <summary>
    /// Possible states of the Character</summary>
    [HideInInspector] public IdleState _idleState;
    [HideInInspector] public AlertState _alertState;
    [HideInInspector] public ChaseState _chaseState;
    [HideInInspector] public AttackState _attackState;
    [HideInInspector] public StunnedState _stunnedState;
    [HideInInspector] public DyingState _dyingState;
    [HideInInspector] public MoveState _moveState;
    [HideInInspector] public BaseState _currentState;

    /// <summary>
    /// The Collider that tracks when other characters enter or leave
    /// the alertness range.</summary>
    public SphereCollider _alertnessCollider;

    /// <summary>
    /// The Collider that represents the actual body of the Character.
    /// It triggers the alertness Collider and functions as a Collider
    /// for actual physics collisions.</summary>
    public Collider _bodyCollider;

    /// <summary>
    /// The NavMeshAgent. Some of the agents properties are used in
    /// calculations of the Character's behaviour.</summary>
    [HideInInspector] public NavMeshAgent _navMeshAgent;

    /// <summary>
    /// The loot prefab that is getting dropped when the Character dies.</summary>
    public Loot _lootPrefab;

    /// <summary>
    /// A list of tags that this Character reacts to in a hostile way.</summary>
    public List<string> _possibleEnemies;

    /// <summary>
    /// Enemies in the attention of the Character.</summary>
    public List<Character> _enemies;

    /// <summary>
    /// Characters that interact with this Character.
    /// This Character might not be aware of the other Characters.
    /// This list is important to make sure that interacting Characters
    /// can notify this Character in the event of their death
    /// or other not yet implemented future events.</summary>
    public List<Character> _interactingCharacters;

    /// <summary>
    /// A timestamp representing the next possible moment for an attack.</summary>
    private float _timeUntilNextAttack;

    /// <summary>
    /// Public access to the Stats.</summary>
    public Stats Stats
    {
        get
        {
            return _stats;
        }
        set
        {
            _stats = value;
            _stats.Character = this;
        }
    }

    /// <summary>
    /// Get the various Components.
    /// Create the possible States.</summary>
    public virtual void Awake ()
    {
        // Get the necessary Components
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _inventory = GetComponent<Inventory>();

        // Initialize necessary properties
        _enemies = new List<Character>();
        _interactingCharacters = new List<Character>();

        // Initialize the Stats
        Stats = Instantiate(_stats);
        Stats.Character = this;

        // Initialize the States
        _idleState = new IdleState(this);
        _alertState = new AlertState(this);
        _chaseState = new ChaseState(this);
        _attackState = new AttackState(this);
        _stunnedState = new StunnedState(this);
        _dyingState = new DyingState(this);
        _moveState = new MoveState(this);
    }

    /// <summary>
    /// The Character Definition has changed, so some of the
    /// Character's values need to be updated.</summary>
    public virtual void CharacterStatsChanged ()
    {
        _alertnessCollider.radius = Stats.AlertnessRange;
        _navMeshAgent.speed = Stats.MovementSpeed;
        _navMeshAgent.angularSpeed = Stats.MovementSpeed * 360;
    }

    /// <summary>
    /// Start off in the Idle State.</summary>
    void Start ()
    {
        _currentState = _idleState;
        CharacterStatsChanged();
    }

    /// <summary>
    /// Update the current State.</summary>
    void Update ()
    {
        _currentState.UpdateState();
    }

    /// <summary>
    /// Change the State to the given state if the given state differs
    /// from the current state.</summary>
    public void ChangeState (BaseState state)
    {
        if (state != _currentState)
        {
            _currentState.ExitState();
            state._previousState = _currentState;
            _currentState = state;
            _currentState.EnterState();
        }
    }

    #if UNITY_EDITOR
    /// <summary>
    /// Draw some Debug helper shapes.</summary>
    void OnDrawGizmos ()
    {
        Color color;
        if (_currentState == _idleState)
        {
            color = Color.green;
        }
        else if (_currentState == _alertState)
        {
            color = Color.yellow;
        }
        else if (_currentState == _chaseState)
        {
            color = Color.magenta;
        }
        else if (_currentState == _attackState)
        {
            color = Color.red;
        }
        else if (_currentState == _stunnedState)
        {
            color = Color.cyan;
        }
        else if (_currentState == _dyingState)
        {
            color = Color.black;
        }
        else if (_currentState == _moveState)
        {
            color = Color.blue;
        }
        else
        {
            color = Color.grey;
        }
        // Show Sight Angle
        Gizmos.color = color;
        var targetA = transform.position + (Quaternion.Euler(0, Stats.SightAngle * 0.5f, 0) * transform.forward * Stats.AlertnessRange);
        Gizmos.DrawLine(transform.position, targetA);
        var targetB = transform.position + (Quaternion.Euler(0, -Stats.SightAngle * 0.5f, 0) * transform.forward * Stats.AlertnessRange);
        Gizmos.DrawLine(transform.position, targetB);
        // Draw a circle for sight range
        DebugExtension.DebugCircle(transform.position, color, Stats.AlertnessRange, 0);
        DebugExtension.DebugCircle(transform.position, color, Stats.AttackRange, 0);
    }
    #endif

    /// <summary>
    /// Registers the character on the Collider, if any.</summary>
    void OnTriggerEnter (Collider other)
    {
        // Go into alert mode if the collider belongs to an enemy
        if (IsBodyCollider(other) && IsEnemy(other.gameObject.tag))
        {
            AddEnemy(other.gameObject.GetComponent<Character>());
        }
    }

    /// <summary>
    /// De-registers the character on the Collider, if any.</summary>
    void OnTriggerExit (Collider other)
    {
        RemoveEnemy(other.gameObject.GetComponent<Character>());
    }

    /// <summary>
    /// Registers the given character into the list of enemies.
    /// If a valid index is provided, the Character will be added at
    /// that index in the list, or moved to the index if the enemy is
    /// already in the list.
    /// If no valid index is given, the enemy will just be added to
    /// the end of the list.
    /// Also informs the enemy that this Character is interacting
    /// with him.</summary>
    /// <param name="enemy">The enemy Character</param>
    /// <param name="index">Desired Index in the list</param>
    public void AddEnemy (Character enemy, int index = -1)
    {
        if (!_enemies.Contains(enemy))
        {
            if (index < 0)
            {
                _enemies.Add(enemy);
            }
            else
            {
                _enemies.Insert(index, enemy);
            }
        }
        else
        {
            if  (index > -1)
            {
                _enemies.Remove(enemy);
                _enemies.Insert(index, enemy);
            }
        }
        enemy.AddInteractingCharacter(this);
    }

    /// <summary>
    /// De-register the Character as an active enemy.</summary>
    public void RemoveEnemy (Character enemy)
    {
        if (_enemies.Contains(enemy))
        {
            _enemies.Remove(enemy);
        }
    }

    /// <summary>
    /// Register the Character as an interacting Character.</summary>
    public void AddInteractingCharacter (Character character)
    {
        if (!_interactingCharacters.Contains(character))
        {
            _interactingCharacters.Add(character);
        }
    }

    /// <summary>
    /// De-register the Character as an interacting Character.</summary>
    public void RemoveInteractingCharacter (Character character)
    {
        if (_interactingCharacters.Contains(character))
        {
            _interactingCharacters.Remove(character);
        }
    }

    /// <summary>
    /// Check if the Collider acts as a Character's body Collider.</summary>
    public static bool IsBodyCollider (Collider collider)
    {
        if (collider.gameObject.GetComponent<Character>() != null)
        {
            return collider.gameObject.GetComponent<Character>()._bodyCollider == collider;
        }
        return false;
    }

    /// <summary>
    /// Whether the tag represents a possible enemy to this Character.</summary>
    public bool IsEnemy (string tag)
    {
        return _possibleEnemies.Contains(tag);
    }

    /// <summary>
    /// De-registers the character on the Collider, if any.</summary>
    public float DistanceToCharater (Character character)
    {
        return Vector3.Magnitude(character.transform.position - transform.position)
               - _navMeshAgent.radius * 0.5f - character._navMeshAgent.radius * 0.5f;
    }

    /// <summary>
    /// Whether the Character is in range for an attack.</summary>
    public bool EnemyInAttackRange (Character enemy)
    {
        return DistanceToCharater(enemy) <= Stats.AttackRange;
    }

    //--------------------------------------------------------------------------
    // Combat related
    //--------------------------------------------------------------------------

    /// <summary>
    /// Whether the Character can attack depends on the time since the
    /// last attack.</summary>
    public bool CanAttack
    {
        get
        {
            return Time.time > _timeUntilNextAttack;
        }
    }

    /// <summary>
    /// A Character is dead once it's current life is below 1.</summary>
    public bool IsDead
    {
        get
        {
            return Stats._currentLife < 1;
        }
    }

    /// <summary>
    /// Attack the given Character.</summary>
    public void Attack (Character enemy)
    {
        enemy.TakeDamage(Stats.Damage, this);
        _timeUntilNextAttack = Time.time + Stats.AttackDuration;
    }

    /// <summary>
    /// Take the given damage from the given attacker.
    /// This Character will loose life and get stunned or dies if
    /// criteria is met.
    /// @TODO Rework this into using a damage object instead.</summary>
    public void TakeDamage (float damage, Character attacker)
    {
        AddEnemy(attacker);
        Stats._currentLife -= (int)damage;
        if (IsDead)
        {
            attacker.OnKilledEnemy(this);
            ChangeState(_dyingState);
            OnDeath();
            return;
        }
        _stunnedState._nextState = _attackState;
        _stunnedState._endTime = Time.time + 0.5f;
        ChangeState(_stunnedState);
        OnTakeDamage();
    }

    /// <summary>
    /// Implemented to receive the death signal of any interacting
    /// Characters.
    /// The Character will be removed from the list of enemies and the
    /// interacting Characters alike.</summary>
    public void InteractingCharacterDied (Character character)
    {
        RemoveEnemy(character);
        RemoveInteractingCharacter(character);
    }

    public virtual void OnKilledEnemy (Character enemy) {}

    public virtual void OnTakeDamage () {}

    public virtual void OnDeath ()
    {
        var loot = (Loot)Instantiate(_lootPrefab, transform.position, transform.rotation);
        loot._inventory.AddInventory(_inventory);
    }

}
