using UnityEngine;
using System.Collections.Generic;
using System;


/// <summary>
/// Attributes defining a Character.</summary>
[Serializable]
public class Stats : ScriptableObject
{
    // Reference to the connected Character
    private Character _character;

    // Basic Attributes
    public string _name;
    public int _experience;
    public int _bodyBase;
    public int _mindBase;
    public int _soulBase;
    public int _currentLife;
    public int _availableAttributePoints;
    public int _availableSkillPoints;

    // Navigation Attributes
    public float _movementSpeed;
    public float _alertnessRangeBase;
    public float _sightAngleBase;

    // Combat Attributes
    public float _attackRangeBase;
    public Weapon _equippedWeapon;
    public Weapon _defaultWeapon;

    // Skills
    public List<Skill> _learnedSkills;
    public List<Skill> _activeSkills;
    public List<Skill> _passiveSkills;

    /// <summary>
    /// The Character holding this Definition.</summary>
    public Character Character
    {
        get
        {
            return _character;
        }
        set
        {
            _character = value;
        }
    }

    //--------------------------------------------------------------------------
    // Base Attributes
    //--------------------------------------------------------------------------

    /// <summary>
    /// The Body Attribute.
    /// @TODO This has to incorporate external modifiers from Skills,
    ///       Items, Effects etc.</summary>
    public int Body
    {
        get        {
            return (int)(_bodyBase * SkillModifier("Body"));
        }
    }

    /// <summary>
    /// The Mind Attribute.
    /// @TODO This has to incorporate external modifiers from Skills,
    ///       Items, Effects etc.</summary>
    public int Mind
    {
        get
        {
            return (int)(_mindBase * SkillModifier("Mind"));
        }
    }

    /// <summary>
    /// The Soul Attribute.
    /// @TODO This has to incorporate external modifiers from Skills,
    ///       Items, Effects etc.</summary>
    public int Soul
    {
        get
        {
            return (int)(_soulBase * SkillModifier("Soul"));
        }
    }

    //--------------------------------------------------------------------------
    // Level
    //--------------------------------------------------------------------------

    /// <summary>
    /// The Level is based on the Experience of the Character.</summary>
    public int Level
    {
        get
        {
            return (int)(Math.Sqrt(_experience/100.0));
        }
    }

    /// <summary>
    /// Absolute Experience for the given Level.</summary>
    private int ExperienceToLevel(int level)
    {
        return 100*(level*level);
    }

    /// <summary>
    /// Absolute Experience to the next Level.</summary>
    public int AbsToNextLevel
    {
        get
        {
            return Math.Abs(_experience - ExperienceToLevel(Level+1));
        }
    }

    /// <summary>
    /// Percentage of the way to gaining the next Level.</summary>
    public float PercentToNextLevel
    {
        get
        {
            return (_experience - ExperienceToLevel(Level)) /
                    ((float)ExperienceToLevel(Level+1) -
                     (float)ExperienceToLevel(Level));
        }
    }

    /// <summary>
    /// The experience for defeating this Character.</summary>
    public int ReturningExperience
    {
        get
        {
            return Level * 10;
        }
    }

    //--------------------------------------------------------------------------
    // Body Attributes
    //--------------------------------------------------------------------------

    /// <summary>
    /// The maximum amount of Life.</summary>
    public int MaxLife
    {
        get
        {
            return 20 + 5*Level + (int)(Body/3.0);
        }
    }

    public float PhysicalResistanceMod
    {
        get
        {
            return 1 + (Body/1000f);
        }
    }

    public float PhysicalDamageMod
    {
        get
        {
            return 1 + (Body/100f);
        }
    }

    //--------------------------------------------------------------------------
    // Mind Attributes
    //--------------------------------------------------------------------------

    // TODO ...

    //--------------------------------------------------------------------------
    // Soul Attributes
    //--------------------------------------------------------------------------

    // TODO ...

    //--------------------------------------------------------------------------
    // Combat Attributes
    //--------------------------------------------------------------------------

    public Weapon EquippedWeapon
    {
        get
        {
            return _equippedWeapon != null ? _equippedWeapon : _defaultWeapon;
        }
        set
        {
            _equippedWeapon = value;
            Character.CharacterStatsChanged();
        }
    }

    public float MovementSpeed
    {
        get
        {
            return _movementSpeed * SkillModifier("MovementSpeed");
        }
    }

    public float AlertnessRange
    {
        get
        {
            return _alertnessRangeBase * SkillModifier("AlertnessRange");
        }
    }

    public float SightAngle
    {
        get
        {
            return _sightAngleBase * SkillModifier("SightAngle");
        }
    }

    public float AttacksPerSecond
    {
        get
        {
            return 1 * EquippedWeapon._speed * SkillModifier("AttacksPerSecond");
        }
    }

    public float AttackDuration
    {
        get
        {
            return 1 / AttacksPerSecond;
        }
    }

    public float AttackRange
    {
        get
        {
            return _attackRangeBase * EquippedWeapon._range * SkillModifier("AttackRange");
        }
    }

    public float Damage
    {
        get
        {
            return PhysicalDamageMod * EquippedWeapon._damage * SkillModifier("Damage");
        }
    }

    public float PhysicalResistance
    {
        get
        {
            return PhysicalResistanceMod * SkillModifier("PhysicalResistance");
        }
    }

    public float SkillModifier (string attribute)
    {
        float mod = 1;
        foreach(Skill skill in _activeSkills)
        {
            if (skill == null)
            {
                continue;
            }
            int pos = Array.IndexOf(skill._modifiedAttributes, attribute);
            if (pos > -1)
            {
                mod += skill.Bonus;
            }
        }
        return mod;
    }

    public void AddSkill (Skill skill)
    {
        _activeSkills.Add(skill);
        Character.CharacterStatsChanged();
    }

    public void RemoveSkill (Skill skill)
    {
        _activeSkills.Remove(skill);
        Character.CharacterStatsChanged();
    }
}
