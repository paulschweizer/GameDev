#if UNITY_EDITOR
using UnityEditor;

/// <summary>
/// Shows all items in an inventory by name and quantity and allows to
/// add and remove Items.</summary>
[CustomEditor(typeof(Stats))]
class StatsEditor : Editor
{

    bool _showEditable = true;
    bool _showBase;
    bool _showLevel;
    bool _showBodySecondary;
    bool _showCombat;
    bool _showNavigation;

    /// <summary>
    /// Draw all the UI elements.</summary>
    public override void OnInspectorGUI()
    {
        var charDef = (Stats)target;

        EditorGUILayout.LabelField("Character Stats: " + charDef._name, EditorStyles.boldLabel);

        _showEditable = EditorGUILayout.Foldout(_showEditable, "Editable Attributes");
        if (_showEditable)
        {
            DrawDefaultInspector();
        }

        // Base Attributes
        _showBase = EditorGUILayout.Foldout(_showBase, "Base Attributes");
        if (_showBase)
        {
            AttributeDisplay("Body", charDef.Body.ToString());
            AttributeDisplay("Mind", charDef.Mind.ToString());
            AttributeDisplay("Soul", charDef.Soul.ToString());
        }

        _showLevel = EditorGUILayout.Foldout(_showLevel, "Level");
        if (_showLevel)
        {
            AttributeDisplay("Level", charDef.Level.ToString());
            AttributeDisplay("Percent to next Level", charDef.PercentToNextLevel.ToString());
            AttributeDisplay("Returning Experience", charDef.ReturningExperience.ToString());
        }

        _showBodySecondary = EditorGUILayout.Foldout(_showBodySecondary, "Body Secondary");
        if (_showBodySecondary)
        {
            AttributeDisplay("Max Life", charDef.MaxLife.ToString());
            AttributeDisplay("Physical Resistance Modifier", charDef.PhysicalResistanceMod.ToString());
            AttributeDisplay("Physical Damage Modifier", charDef.PhysicalDamageMod.ToString());
        }

        _showNavigation = EditorGUILayout.Foldout(_showNavigation, "Navigation");
        if (_showNavigation)
        {
            AttributeDisplay("Movement Speed", charDef.MovementSpeed.ToString());
            AttributeDisplay("Alertness Range", charDef.AlertnessRange.ToString());
            AttributeDisplay("Sight Angle", charDef.SightAngle.ToString());
        }

        _showCombat = EditorGUILayout.Foldout(_showCombat, "Combat");
        if (_showCombat)
        {
            AttributeDisplay("Attacks Per Second", charDef.AttacksPerSecond.ToString());
            AttributeDisplay("Attack Duration", charDef.AttackDuration.ToString());
            AttributeDisplay("Attack Range", charDef.AttackRange.ToString());
            AttributeDisplay("Damage", charDef.Damage.ToString());
            AttributeDisplay("Physical Resistance", charDef.PhysicalResistance.ToString());
        }
    }

    void AttributeDisplay (string label, string value)
    {
        EditorGUILayout.BeginHorizontal("box");
        EditorGUILayout.LabelField(label + ":");
        EditorGUILayout.LabelField(value, EditorStyles.boldLabel);
        EditorGUILayout.EndHorizontal();
    }

}
#endif
