using UnityEngine;

/// <summary>
/// Chase an enemy.</summary>
public class ChaseState : BaseState
{
    /// <summary>
    /// Constructor assigns the Character.</summary>
    public ChaseState (Character character)
    {
        _character = character;
    }

    /// <summary>
    /// Resume the NavMeshAgent.</summary>
    public override void EnterState ()
    {
        _character._navMeshAgent.Resume();
    }

    /// <summary>
    /// If the enemy is still in alertness range, chase him, otherwise
    /// stay alert and look for other targets.
    /// If the enemy is reached, go into attack state.</summary>
    public override void UpdateState ()
    {
        if (_character._enemies.Count == 0)
        {
            _character.ChangeState(_character._alertState);
            return;
        }

        _character._navMeshAgent.destination = _character._enemies[0].transform.position;

        if (_character.EnemyInAttackRange(_character._enemies[0]))
        {
            _character.ChangeState(_character._attackState);
        }
    }
}
