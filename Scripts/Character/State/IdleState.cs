
/// <summary>
/// The initial State of a Character.</summary>
public class IdleState : BaseState
{
    /// <summary>
    /// Constructor assigns the Character.</summary>
    public IdleState(Character character)
    {
        _character = character;
    }

    /// <summary>
    /// Stop the NavMeshAgent.</summary>
    public override void EnterState ()
    {
        _character._navMeshAgent.Stop();
    }

    /// <summary>
    /// If any Characters have entered the alertness range,
    /// start looking for them.</summary>
    public override void UpdateState ()
    {
        if (_character._enemies.Count > 0)
        {
            _character.ChangeState(_character._alertState);
        }
    }
}
