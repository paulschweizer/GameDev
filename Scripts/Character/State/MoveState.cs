using UnityEngine;

/// <summary>
/// The Character moves to a desired destination.</summary>
public class MoveState : BaseState
{
    /// <summary>
    /// Constructor assigns the Character.</summary>
    public MoveState(Character character)
    {
        _character = character;
    }

    /// <summary>
    /// Resume the NavMeshAgent.</summary>
    public override void EnterState ()
    {
        _character._navMeshAgent.Resume();
    }

    /// <summary>
    /// Switch to IdleState, if the target is reached.</summary>
    public override void UpdateState ()
    {
       if (_character._navMeshAgent.remainingDistance <= _character._navMeshAgent.stoppingDistance)
        {
            if (!_character._navMeshAgent.hasPath || Mathf.Abs(_character._navMeshAgent.velocity.sqrMagnitude) < float.Epsilon)
            {
                _character.ChangeState(_character._idleState);
            }
        }
    }
}
