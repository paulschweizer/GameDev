using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// The character is dead.</summary>
public class DyingState : BaseState
{
    /// <summary>
    /// Constructor assigns the Character.</summary>
    public DyingState(Character character)
    {
        _character = character;
    }

    /// <summary>
    /// Make all the interacting Characters aware of the Death of this
    /// Character and destroy the GameObject at the end.</summary>
    public override void EnterState ()
    {
        _character.gameObject.SetActive(false);
        foreach (Character character in _character._interactingCharacters)
        {
            ExecuteEvents.ExecuteHierarchy<IStateEvents>(character.gameObject, null, (x, y) => x.InteractingCharacterDied(_character));
        }
        GameObject.Destroy(_character.gameObject);
    }
}
