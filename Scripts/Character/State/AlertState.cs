using UnityEngine;

/// <summary>
/// An alert Character tries to spot an enemy within his alertness range.</summary>
public class AlertState : BaseState
{
    /// <summary>
    /// Constructor assigns the Character.</summary>
    public AlertState (Character character)
    {
        _character = character;
    }

    /// <summary>
    /// Stops the NavMeshAgent.</summary>
    public override void EnterState ()
    {
        _character._navMeshAgent.Stop();
    }

    /// <summary>
    /// If there are still enemies in range, keep looking for them.
    /// If an enemy is spotted, chase the enemy.</summary>
    public override void UpdateState ()
    {
        if (_character._enemies.Count == 0)
        {
            _character.ChangeState(_character._idleState);
            return;
        }
        Character enemy;
        if (EnemyInSight(out enemy))
        {
            _character.AddEnemy(enemy, 0);
            _character.ChangeState(_character._chaseState);
            return;
        }
        _character.transform.Rotate(0, (_character._navMeshAgent.angularSpeed / 20) * Time.deltaTime, 0);
    }

    /// <summary>
    /// Whether any enemy is in sight range.
    /// Also checks whether the assigned enemies are still in alertness
    /// range and de-registers them if not.</summary>
    public bool EnemyInSight (out Character enemyInSight)
    {
        for (int i = _character._enemies.Count-1; i >= 0; i--)
        {
            var enemy = _character._enemies[i];
            var vectorToEnemy = enemy.transform.position - _character.transform.position;
            if (_character.DistanceToCharater(enemy) > _character.Stats.AlertnessRange)
            {
                _character.RemoveEnemy(enemy);
                continue;
            }
            float angle = Vector3.Angle(vectorToEnemy.normalized, _character.transform.forward);
            if (angle < _character.Stats.SightAngle / 2)
            {
                enemyInSight = enemy;
                return true;
            }
        }
        enemyInSight = null;
        return false;
    }
}
