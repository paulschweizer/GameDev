

/// <summary>
/// Character is in a fight and attacks an enemy.</summary>
public class AttackState : BaseState
{
    /// <summary>
    /// Constructor assigns the Character.</summary>
    public AttackState(Character character)
    {
        _character = character;
    }

    /// <summary>
    /// Stops the NavMeshAgent.</summary>
    public override void EnterState ()
    {
        _character._navMeshAgent.Stop();
    }

    /// <summary>
    /// IF the enemy is within attack range, try to attack him,
    /// otherwise, start chasing him.
    /// If there are no more enemies in range, switch to alert state.</summary>
    public override void UpdateState ()
    {
        if (_character._enemies.Count == 0)
        {
            _character.ChangeState(_character._alertState);
            return;
        }

        // Look at enemy
        _character.transform.LookAt(_character._enemies[0].transform);

        // If not in Attack Range any more, chase the enemy
        if (!_character.EnemyInAttackRange(_character._enemies[0]))
        {
            _character.ChangeState(_character._chaseState);
            return;
        }

        // Attack
        if (_character.CanAttack)
        {
            _character.Attack(_character._enemies[0]);
        }
    }
}
