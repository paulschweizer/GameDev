using UnityEngine;

/// <summary>
/// The Character is stunned and can't react until the timer
/// has run out.</summary>
public class StunnedState : BaseState
{
    /// <summary>
    /// The time at which the stunning ends.</summary>
    public float _endTime;

    /// <summary>
    /// Constructor assigns the Character.</summary>
    public StunnedState(Character character)
    {
        _character = character;
    }

    /// <summary>
    /// Overridden to prevent any user input.</summary>
    public override void HandleInput () {}

    /// <summary>
    /// Stops the NavMeshAgent.</summary>
    public override void EnterState ()
    {
        _character._navMeshAgent.Stop();
    }

    /// <summary>
    /// Switch to the next State when the ending time has been reached.</summary>
    public override void UpdateState ()
    {
        if (Time.time >= _endTime)
        {
            _character.ChangeState(_nextState);
        }
    }

    /// <summary>
    /// Reset the ending time.</summary>
    public override void ExitState ()
    {
        _endTime = 0;
    }
}
