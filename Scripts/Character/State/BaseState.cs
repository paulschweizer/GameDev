using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Base abstract class for all derived States.</summary>
public abstract class BaseState
{
    /// <summary>
    /// The character holding this State.</summary>
    public Character _character;

    /// <summary>
    /// The previous State.
    /// Not really in use anywhere as of now.</summary>
    public BaseState _previousState;

    /// <summary>
    /// The anticipated next state.
    /// Only in use for the Stunned State as of now.</summary>
    public BaseState _nextState;

    /// <summary>
    /// Called when entering the State.</summary>
    public virtual void EnterState () {}

    /// <summary>
    /// Called every frame when the State is active.</summary>
    public virtual void UpdateState () {}

    /// <summary>
    /// Called before changing to the next State.</summary>
    public virtual void ExitState () {}

    /// <summary>
    /// Handle User input to the Character.
    /// Basic implementation handles the Moving and Attacking based on
    /// the User input.</summary>
    public virtual void HandleInput ()
    {
        RaycastHit hit = HitFromMouse();
        // Idle State if hit outside of terrain
        if (hit.collider == null)
        {
            return;
        }
        if (_character.IsEnemy(hit.collider.tag))
        {
            HitEnemy(hit);
            return;
        }
        if (hit.collider.CompareTag("Terrain"))
        {
            HitTerrain(hit);
        }
    }

    /// <summary>
    /// Raycast the user input to retrieve a possible hit.</summary>
    private RaycastHit HitFromMouse ()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        Physics.Raycast(ray, out hit, 100, LayerMask.GetMask("Characters",
                                                             "Terrain"));
        return hit;
    }

    /// <summary>
    /// Change to attack or chase State when an enemy was hit with
    /// user input.
    /// Also set the enemy as the first enemy in the list of enemies.</summary>
    private void HitEnemy (RaycastHit hit)
    {
        Character enemy = hit.collider.gameObject.GetComponent<Character>();
        _character.AddEnemy(enemy, 0);
        if (_character.EnemyInAttackRange(enemy))
        {
            _character.ChangeState(_character._attackState);
        }
        else
        {
            _character.ChangeState(_character._chaseState);
        }
    }

    /// <summary>
    /// Move to the hit destination on the terrain.</summary>
    private void HitTerrain (RaycastHit hit)
    {
        _character._navMeshAgent.SetDestination(hit.point);
        _character.ChangeState(_character._moveState);
    }
}


/// <summary>
/// Interface for Characters to react to certain events during the
/// State execution.</summary>
public interface IStateEvents : IEventSystemHandler
{
    /// <summary>
    /// Send a signal to all interacting Characters when this Character
    /// dies.</summary>
    void InteractingCharacterDied (Character character);
}
