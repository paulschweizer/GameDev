using UnityEngine;
using System;
using System.Collections.Generic;

/// <summary>
/// Character controlled by the Player.</summary>
[Serializable]
public class Player : Character
{
    /// <summary>
    /// Static reference to the Player.</summary>
    public static Player _player;

    //--------------------------------------------------------------------------
    // mono methods
    //--------------------------------------------------------------------------

    /// <summary>
    /// Keep the Player unique.</summary>
    public override void Awake ()
    {
        if (_player == null)
        {
            _player = this;
        }
        else if(_player != null)
        {
            Destroy(gameObject);
        }
        base.Awake();
    }

    //--------------------------------------------------------------------------
    // overridden methods
    //--------------------------------------------------------------------------

    /// <summary>
    /// The Character Definition has changed, so some of the
    /// Character's values need to be updated.</summary>
    public override void CharacterStatsChanged ()
    {
        _alertnessCollider.radius = Stats.AlertnessRange;
        _navMeshAgent.speed = Stats.MovementSpeed;
        _navMeshAgent.angularSpeed = Stats.MovementSpeed * 360;
        PlayerController.UpdateAllViews();
    }

    /// <summary>
    /// Gain experience from the killed enemy.</summary>
    public override void OnKilledEnemy (Character enemy)
    {
        PlayerController.GainExperience(enemy.Stats.ReturningExperience);
    }

    /// <summary>
    /// Notify the PlayerController that Damage has been taken.</summary>
    public override void OnTakeDamage ()
    {
        PlayerController.TakeDamage();
    }

    /// <summary>
    /// Notify the PlayerController that the Character has died.</summary>
    public override void OnDeath ()
    {
        PlayerController.Death();
    }

    //--------------------------------------------------------------------------
    // public methods
    //--------------------------------------------------------------------------

    /// <summary>
    /// Attack the closest enemy in alertness range.
    /// @TODO Use a larger range, maybe the entire screen.</summary>
    public void AttackClosestEnemy ()
    {
        if (_enemies.Count == 0)
        {
            return;
        }
        if (_currentState == _attackState)
        {
            return;
        }

        float distance = 0f;
        Character closest = null;
        for (int i = 0; i < _enemies.Count; i++)
        {
            var enemy = _enemies[i];
            float distToEnemy = DistanceToCharater(enemy);
            if (i == 0)
            {
                distance = distToEnemy;
                closest = enemy;
            }
            else
            {
                if (distToEnemy < distance)
                {
                    distance = distToEnemy;
                    closest = enemy;
                }
            }
            if (i+1 == _enemies.Count)
            {
                AddEnemy(closest, 0);
                ChangeState(_chaseState);
            }
        }
    }

    //--------------------------------------------------------------------------
    // Serialization
    //--------------------------------------------------------------------------

    /// <summary>
    /// Save the Player when disabled.</summary>
    void OnDisable ()
    {
        if (!IsDead)
        {
            MainController.SaveData("Player", Serialize());
        }
    }

    /// <summary>
    /// Create a serializable representation of this class.</summary>
    public SerializedPlayer Serialize ()
    {
        float[] position = new float[3] {transform.position.x,
                                         transform.position.y,
                                         transform.position.z};
        List<int> items = new List<int>();
        foreach (InventoryItem item in _inventory._items)
        {
            items.Add(item._id);
        }
        List<int> learnedSkills = new List<int>();
        foreach (Skill skill in Stats._learnedSkills)
        {
            learnedSkills.Add(skill._id);
        }
        List<int> activeSkills = new List<int>();
        foreach (Skill skill in Stats._activeSkills)
        {
            activeSkills.Add(skill._id);
        }
        List<int> passiveSkills = new List<int>();
        foreach (Skill skill in Stats._passiveSkills)
        {
            passiveSkills.Add(skill._id);
        }
        return new SerializedPlayer(position,
                                    Stats._name,
                                    Stats._experience,
                                    Stats._bodyBase,
                                    Stats._mindBase,
                                    Stats._soulBase,
                                    Stats._currentLife,
                                    Stats._availableAttributePoints,
                                    Stats._availableSkillPoints,
                                    Stats._movementSpeed,
                                    Stats._alertnessRangeBase,
                                    Stats._sightAngleBase,
                                    Stats._attackRangeBase,
                                    items,
                                    _inventory._quantities,
                                    Stats.EquippedWeapon._id,
                                    learnedSkills,
                                    activeSkills,
                                    passiveSkills);
    }

    /// <summary>
    /// Create a serializable representation of this class.</summary>
    public void DeSerialize (SerializedPlayer serializedPlayer)
    {
        transform.position = new Vector3(serializedPlayer._position[0],
                                         serializedPlayer._position[1],
                                         serializedPlayer._position[2]);

        Stats._name = serializedPlayer._name;
        Stats._experience = serializedPlayer._experience;
        Stats._bodyBase = serializedPlayer._bodyBase;
        Stats._mindBase = serializedPlayer._mindBase;
        Stats._soulBase = serializedPlayer._soulBase;
        Stats._currentLife = serializedPlayer._currentLife;
        Stats._availableAttributePoints = serializedPlayer._availableAttributePoints;
        Stats._availableSkillPoints = serializedPlayer._availableSkillPoints;
        Stats._movementSpeed = serializedPlayer._movementSpeed;
        Stats._alertnessRangeBase = serializedPlayer._alertnessRangeBase;
        Stats._sightAngleBase = serializedPlayer._sightAngleBase;
        Stats._attackRangeBase = serializedPlayer._attackRangeBase;

        for (int i = 0; i < serializedPlayer._items.Count; i++)
        {
            var item = ItemDatabase.GetItemById(serializedPlayer._items[i]);
            _inventory.AddItem(item, serializedPlayer._quantities[i]);
        }

        Stats.EquippedWeapon = (Weapon)ItemDatabase.GetItemById(serializedPlayer._equippedWeapon);
        foreach (int id in serializedPlayer._learnedSkills)
        {
            Skill skill = SkillDatabase.GetSkillById(id);
            Stats._learnedSkills.Add(skill);
            Stats.AddSkill(skill);
        }
        foreach (int id in serializedPlayer._activeSkills)
        {
            Skill skill = SkillDatabase.GetSkillById(id);
            Stats._activeSkills.Add(skill);
            Stats.AddSkill(skill);
        }
        foreach (int id in serializedPlayer._passiveSkills)
        {
            Skill skill = SkillDatabase.GetSkillById(id);
            Stats._passiveSkills.Add(skill);
            Stats.AddSkill(skill);
        }
    }
}


[Serializable]
public class SerializedPlayer
{
    [SerializeField] public float[] _position;

    [SerializeField] public string _name;
    [SerializeField] public int _experience;
    [SerializeField] public int _bodyBase;
    [SerializeField] public int _mindBase;
    [SerializeField] public int _soulBase;
    [SerializeField] public int _currentLife;
    [SerializeField] public int _availableAttributePoints;
    [SerializeField] public int _availableSkillPoints;

    // Navigation Attributes
    [SerializeField] public float _movementSpeed;
    [SerializeField] public float _alertnessRangeBase;
    [SerializeField] public float _sightAngleBase;

    // Combat Attributes
    [SerializeField] public float _attackRangeBase;

    // Inventory
    [SerializeField] public List<int> _items;
    [SerializeField] public List<int> _quantities;

    [SerializeField] public int _equippedWeapon;

    // Skills
    [SerializeField] public List<int> _learnedSkills;
    [SerializeField] public List<int> _activeSkills;
    [SerializeField] public List<int> _passiveSkills;

    /// <summary>
    /// Initialize the SerializablePlayer.</summary>
    public SerializedPlayer (float[] position,
                             string name,
                             int experience,
                             int bodyBase,
                             int mindBase,
                             int soulBase,
                             int currentLife,
                             int availableAttributePoints,
                             int availableSkillPoints,
                             float movementSpeed,
                             float alertnessRangeBase,
                             float sightAngleBase,
                             float attackRangeBase,
                             List<int> items,
                             List<int> quantities,
                             int equippedWeapon,
                             List<int> learnedSkills,
                             List<int> activeSkills,
                             List<int> passiveSkills)
    {
        _position = position;
        _name = name;
        _experience = experience;
        _bodyBase = bodyBase;
        _mindBase = mindBase;
        _soulBase = soulBase;
        _currentLife = currentLife;
        _availableAttributePoints = availableAttributePoints;
        _availableSkillPoints = availableSkillPoints;
        _movementSpeed = movementSpeed;
        _alertnessRangeBase = alertnessRangeBase;
        _sightAngleBase = sightAngleBase;
        _attackRangeBase = attackRangeBase;
        _items = items;
        _quantities = quantities;
        _equippedWeapon = equippedWeapon;
        _learnedSkills = learnedSkills;
        _activeSkills = activeSkills;
        _passiveSkills = passiveSkills;
    }
}
