#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

/// <summary>
/// Editor Menu for Characters.</summary>
public class CharacterEditorMenu : EditorWindow
{

    string _name;

    [MenuItem ("Shadowhunter/New Character Stats")]
    static void Init ()
    {
        // Get existing open window or if none, make a new one:
        var window = (CharacterEditorMenu)EditorWindow.GetWindow(typeof(CharacterEditorMenu));
        window.Show();
    }

    void OnGUI ()
    {
        _name = EditorGUILayout.TextField ("Name", _name);
        // Create the Item
        if (GUILayout.Button("Create Character", GUILayout.Height(30)))
        {
            CreateNewItem();
        }
    }

    void CreateNewItem ()
    {
        var character = ScriptableObject.CreateInstance<Stats>();
        character._name = _name;
        AssetDatabase.CreateAsset(character, "Assets/Content/Characters/" + character._name + ".asset");
        AssetDatabase.SaveAssets();

    }
}
#endif
