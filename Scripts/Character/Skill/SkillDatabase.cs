using UnityEngine;
using System.Collections.Generic;

public class SkillDatabase: MonoBehaviour
{
    public static SkillDatabase _skillDatabase;
    public List<Skill> _skills;

    protected void Awake ()
    {
        if (_skillDatabase == null)
        {
            DontDestroyOnLoad(gameObject);
            _skillDatabase = this;
        }
        else if(_skillDatabase != null)
        {
            Destroy(gameObject);
        }
    }

    public static List<Skill> Skills {
        get
        {
            return _skillDatabase._skills;
        }
    }

    public static Skill GetSkillById (int id)
    {
        foreach (Skill skill in _skillDatabase._skills)
        {
            if (skill._id == id)
            {
                return skill;
            }
        }
        return null;
    }
}
