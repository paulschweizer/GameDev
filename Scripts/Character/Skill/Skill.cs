using UnityEngine;
using System;

[Serializable]
public class Skill : ScriptableObject
{
    public int _id;

    public string _name;

    [Multiline]
    public string _description;

    public Skill[] _requiredSkills;
    public int _requiredLevel;
    public int _cost;

    public string[] _modifiedAttributes;
    public float _bonus;

    public float _cooldown_time;
    public bool _active;

    public float Bonus
    {
        get
        {
            return _bonus;
        }
    }

    public void Use ()
    {
        Debug.Log("Using Skill '" + _name + "'");
    }
}
