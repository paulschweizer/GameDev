#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.IO;

/// <summary>
/// Editor Menu for the Inventory.</summary>
public class SkillEditorMenu : EditorWindow
{
    string _name;
    string _description;

    [MenuItem ("Shadowhunter/New Skill")]
    static void Init ()
    {
        // Get existing open window or if none, make a new one:
        var window = (SkillEditorMenu)EditorWindow.GetWindow(typeof(SkillEditorMenu));
        window.Show();
    }

    void OnGUI ()
    {
        _name = EditorGUILayout.TextField ("Name", _name);
        EditorGUILayout.LabelField("Description");
        _description = EditorGUILayout.TextField(_description, GUILayout.Height(50));

        // Create the Skill
        if (GUILayout.Button("Create Skill", GUILayout.Height(30)))
        {
            CreateNewSkill();
        }
    }

    void CreateNewSkill ()
    {
        var levelDirectoryPath = new DirectoryInfo(Application.dataPath + "/Content/Skills");
        FileInfo[] assets = levelDirectoryPath.GetFiles("*.asset", SearchOption.AllDirectories);

        var skill = ScriptableObject.CreateInstance<Skill>();
        skill._id = assets.Length;
        skill._name = _name;
        skill._description = _description;
        AssetDatabase.CreateAsset(skill, "Assets/Content/Skills/" + skill._name + ".asset");
        AssetDatabase.SaveAssets();
    }
}
#endif
