using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// View for game settings .</summary>
public class GameView: MonoBehaviour
{
    #if UNITY_WEBPLAYER
    public static string webplayerQuitURL = "http://google.com";
    #endif

    public void ReturnToMainMenu ()
    {
        MainController.SwitchScene("Scenes/MenuScene");
    }

    public void SaveAndExit ()
    {
        MainController.Exit();
    }
}
