using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// View for the Character Stats.</summary>
public class CharacterView : MonoBehaviour
{
    /// <summary>
    /// TextView for the available attribute points.</summary>
    public Text _availableAttributePoints;

    /// <summary>
    /// The body attribute.</summary>
    public Text _body;

    /// <summary>
    /// The mind attribute.</summary>
    public Text _mind;

    /// <summary>
    /// The soul attribute.</summary>
    public Text _soul;

    /// <summary>
    /// The available skill points.</summary>
    public Text _availableSkillPoints;

    /// <summary>
    /// The Panel holding the skils.</summary>
    public RectTransform _skillPanel;

    /// <summary>
    /// Prefab for the SkillView.</summary>
    public SkillView _skillViewPrefab;

    //--------------------------------------------------------------------------
    // Mono methods
    //--------------------------------------------------------------------------

    /// <summary>
    /// - Update the Attribute Panel
    /// - Add all available Skills</summary>
    void Start ()
    {
        UpdateAvailableAttributePoints();
        UpdateAttribute();

        foreach (Skill skill in SkillDatabase.Skills)
        {
            SkillView skillView = Instantiate(_skillViewPrefab);
            skillView.Init(skill);
            skillView.transform.SetParent(_skillPanel);
            skillView.transform.localScale = new Vector3(1, 1, 1);
        }
    }

    //--------------------------------------------------------------------------
    // Actions
    //--------------------------------------------------------------------------

    /// <summary>
    /// Raise the given attribute if attribute points are available.</summary>
    public void SpendAttributePoint (string attribute)
    {
        if (Player._player.Stats._availableAttributePoints > 0)
        {
            if (attribute == "body")
            {
                Player._player.Stats._bodyBase += 1;
            }
            if (attribute == "mind")
            {
                Player._player.Stats._mindBase += 1;
            }
            if (attribute == "soul")
            {
                Player._player.Stats._soulBase += 1;
            }
            Player._player.Stats._availableAttributePoints -= 1;
            UpdateAvailableAttributePoints();
            UpdateAttribute();
        }
    }

    //--------------------------------------------------------------------------
    // Updates
    //--------------------------------------------------------------------------

    /// <summary>
    /// Update the entire menu from the character stats.</summary>
    public void UpdateEntireMenu ()
    {
        UpdateAvailableAttributePoints();
        UpdateAvailableSkillPoints();
        UpdateAttribute();
    }

    /// <summary>
    /// Update the available attribute points.</summary>
    public void UpdateAvailableAttributePoints ()
    {
        _availableAttributePoints.text = "Available Attribute Points: " + Player._player.Stats._availableAttributePoints;
    }

    /// <summary>
    /// Update the available skill points.</summary>
    public void UpdateAvailableSkillPoints ()
    {
        _availableSkillPoints.text = "Available Skill Points: " + Player._player.Stats._availableSkillPoints;
    }

    /// <summary>
    /// Update the Attribute Texts.
    /// @TODO Change the display color and such based on the difference in value.</summary>
    public void UpdateAttribute ()
    {
        _body.text = "Body: " + Player._player.Stats.Body + "/" + Player._player.Stats._bodyBase;
        _mind.text = "Mind: " + Player._player.Stats.Mind + "/" + Player._player.Stats._mindBase;
        _soul.text = "Soul: " + Player._player.Stats.Soul + "/" + Player._player.Stats._soulBase;
    }
}
