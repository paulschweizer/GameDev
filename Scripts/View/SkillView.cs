using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// UI View for a Skill.</summary>
public class SkillView : MonoBehaviour
{
    /// <summary>
    /// The name.</summary>
    public Text _name;

    /// <summary>
    /// The bonus.</summary>
    public Text _bonus;

    /// <summary>
    /// The description.</summary>
    public Text _description;

    /// <summary>
    /// The button to learn the Skill.</summary>
    public GameObject _learnButton;

    /// <summary>
    /// The Text on the learn button.</summary>
    public Text _learnButtonText;

    /// <summary>
    /// Image in case the Skill has been learned.</summary>
    public GameObject _learnedImage;

    /// <summary>
    /// The Skill displayed by this View.</summary>
    public Skill _skill;

    //--------------------------------------------------------------------------
    // public methods
    //--------------------------------------------------------------------------

    /// <summary>
    /// Initialize the SkillView.</summary>
    public void Init (Skill skill)
    {
        _skill = skill;
        _name.text = skill._name;
        _bonus.text = skill._modifiedAttributes[0] + ": " + skill._bonus;
        _description.text = skill._description;

        if (Player._player.Stats._learnedSkills.Contains(skill))
        {
            _learnButton.SetActive(false);
            _learnedImage.SetActive(true);
        }
        else
        {
            _learnButton.SetActive(true);
            _learnedImage.SetActive(false);
            _learnButtonText.text = skill._cost.ToString();
        }
    }

    //--------------------------------------------------------------------------
    // Actions
    //--------------------------------------------------------------------------

    /// <summary>
    /// The button to learn the Skill.
    /// Informs the PlayerCharacter of the change.</summary>
    public void LearnSkill ()
    {
        if (Player._player.Stats._availableSkillPoints >=
            _skill._cost)
        {
            _learnButton.SetActive(false);
            _learnedImage.SetActive(true);
            PlayerController.LearnSkill(_skill);
        }
    }
}
