using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// The in-game Menu for the Player.</summary>
public class PlayerMenu : MonoBehaviour
{
    /// <summary>
    /// The PlayerMenu as a static.</summary>
    public static PlayerMenu _playerMenu;

    /// <summary>
    /// Slider showing the Player's life.</summary>
    public Slider _lifeSlider;

    /// <summary>
    /// Slider showing the Player's experience.</summary>
    public Slider _experienceSlider;

    /// <summary>
    /// The Main Menu.</summary>
    public GameObject _menu;

    /// <summary>
    /// Reference to the CharacterView.</summary>
    public CharacterView _characterView;

    /// <summary>
    /// Keep the PlayerMenu a Singleton.</summary>
    void Awake ()
    {
        if (_playerMenu == null)
        {
            _playerMenu = this;
        }
        else if(_playerMenu != null)
        {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Make sure the menu is hidden on start.</summary>
    void Start ()
    {
        _menu.SetActive(false);
    }

    //--------------------------------------------------------------------------
    // Actions
    //--------------------------------------------------------------------------

    /// <summary>
    /// Show the Menu.</summary>
    public void ShowMenu ()
    {
        _menu.SetActive(!_menu.activeSelf);
    }

    /// <summary>
    /// Attack the closest enemy.</summary>
    public void ClickedAttackButton ()
    {
        Player._player.AttackClosestEnemy();
    }

    //--------------------------------------------------------------------------
    // Updates
    //--------------------------------------------------------------------------

    /// <summary>
    /// Update the entire menu.</summary>
    public void UpdateEntireMenu ()
    {
        UpdateLifeSlider();
        UpdateExperienceSlider();
        _characterView.UpdateEntireMenu();
    }

    /// <summary>
    /// Set the life Slider to the current life value.</summary>
    public void UpdateLifeSlider ()
    {
        _lifeSlider.maxValue = Player._player.Stats.MaxLife;
        _lifeSlider.value = Player._player.Stats._currentLife;
    }

    /// <summary>
    /// Set the experience Slider to show the leveling progress.</summary>
    public void UpdateExperienceSlider ()
    {
        _experienceSlider.value = Player._player.Stats.PercentToNextLevel;
    }
}
