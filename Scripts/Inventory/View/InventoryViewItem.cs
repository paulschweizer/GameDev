using UnityEngine.UI;
using SlotSystem;

/// <summary>
/// Visual Item for the InventoryView.</summary>
public class InventoryViewItem : SlottableItem
{
    /// <summary>
    /// Reference to the InventoryItem.</summary>
    public InventoryItem _item;

    /// <summary>
    /// The ModelItem holding the quantity.</summary>
    public int _quantity;

    /// <summary>
    /// Text Field to display the name.</summary>
    public Text _name;

    /// <summary>
    /// Text Field to display the quantity of the Item.</summary>
    public Text _quantityText;

    /// <summary>
    /// Initialize the visual UI elements with the information from the
    /// InventoryItem</summary>
    public void Init (InventoryItem item, int quantity)
    {
        _item = item;
        _quantity = quantity;
        UpdateDisplay();
    }

    /// <summary>
    /// Refresh the display of the Item. Called when the quantity of
    /// the Item has changed.</summary>
    public void UpdateDisplay ()
    {
        _name.text = _item._name;
        _quantityText.text = _quantity.ToString();
    }

    /// <summary>
    /// The InventoryItem.</summary>
    public override object Data
    {
        get
        {
            return _item;
        }
        set
        {
            _item = (InventoryItem)value;
        }
    }
}
