using UnityEngine;
using UnityEngine.EventSystems;
using SlotSystem;

/// <summary>
/// Slot to hold an InventoryItem in the InventoryView.</summary>
public class InventorySlot : Slot
{
    /// <summary>
    /// Accepted Item types for this slot.
    /// If left empty, all Item types are accepted.
    /// @TODO Maybe use a list instead?</summary>
    public string _acceptedItemType;

    /// <summary>
    /// Weather the Slot accepts the given Item.</summary>
    public override bool AcceptsItem (SlottableItem item)
    {
        if (_acceptedItemType == "" || _acceptedItemType == item.Data.GetType().Name)
        {
            return true;
        }
        return false;
    }
}
