using System.Collections.Generic;
using SlotSystem;

/// <summary>
/// View for the Slot based Inventory.
/// This Inventory View is fixed to be used by the PlayerCharacter.</summary>
public class InventoryView : SlotView
{
    /// <summary>
    /// Prefab for the Item.</summary>
    public InventoryViewItem _viewItem;

    /// <summary>
    /// A slot for the equipped weapon.</summary>
    public InventorySlot _weaponSlot;

    /// <summary>
    /// To keep track of the available Items in the View.</summary>
    private Dictionary<InventoryItem, InventoryViewItem> _items = new Dictionary<InventoryItem, InventoryViewItem> () {};

    /// <summary>
    /// Show the View.</summary>
    public bool Show ()
    {
        gameObject.SetActive(!gameObject.activeSelf);
        return gameObject.activeSelf;
    }

    /// <summary>
    /// Update the Character in case the Item is placed in a special
    /// Slot, like the Weapon Slot.</summary>
    public override void SlotChanged (Slot slot,
                                      SlottableItem currentItem,
                                      SlottableItem previousItem)
    {
        if (slot == _weaponSlot)
        {
            Player._player.Stats._equippedWeapon = currentItem != null ? (Weapon)currentItem.Data : null;
        }
    }

    /// <summary>
    /// Add an Item to the View.
    /// If it already exists, update the quantity.</summary>
    public InventoryViewItem AddItem (InventoryItem item, int quantity)
    {
        InventoryViewItem viewItem;
        if (_items.TryGetValue(item, out viewItem))
        {
            viewItem._quantity = quantity;
            viewItem.UpdateDisplay();
        }
        else
        {
            viewItem = Instantiate(_viewItem);
            viewItem.Init(item, quantity);
            AddItem(viewItem);
            _items[item] = viewItem;
        }
        return viewItem;
    }

    /// <summary>
    /// Delete the given Item.</summary>
    public void RemoveItem (InventoryItem item)
    {
        var invItem = _items[item];
        _items.Remove(item);
        Destroy(invItem.gameObject);
    }

    /// <summary>
    /// Change and updates the Display data on the Item.</summary>
    public void ChangeItemData (InventoryItem item, int quantity)
    {
        var invItem = _items[item];
        invItem._quantity = quantity;
        invItem.UpdateDisplay();
    }

    /// <summary>
    /// Reset and initialize the entire view based on the given Inventory.</summary>
    public void InitFromInventory (Inventory inventory)
    {
        foreach (KeyValuePair<InventoryItem, InventoryViewItem> entry in _items)
        {
            Destroy(entry.Value.gameObject);
        }
        _items.Clear();
        _numberOfSlots = inventory._numberOfSlots;
        ResetSlots();
        for (int i=0; i < inventory._items.Count; i++)
        {
            var invItem = AddItem(inventory._items[i], inventory._quantities[i]);
            // Update the weapon Slot
            if (inventory._items[i] == Player._player.Stats._equippedWeapon)
            {
                _weaponSlot.Swap(invItem);
            }
        }
    }
}
