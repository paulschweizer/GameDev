using UnityEngine;
using System.Collections.Generic;

namespace SlotSystem
{
    /// <summary>
    /// View for the SlotSystem.</summary>
    public class SlotView : MonoBehaviour, ISlotChanged
    {
        /// <summary>
        /// Number of available Slots</summary>
        public int _numberOfSlots;

        /// <summary>
        /// Slot Prefab</summary>
        public bool _infiniteSlots;

        /// <summary>
        /// Slot Prefab</summary>
        public Slot _slot;

        /// <summary>
        /// Instantiated Slots</summary>
        private readonly List<Slot> _slots = new List<Slot>();

        /// <summary>
        /// TParent for the Slots.</summary>
        public Transform _slotParent;

        /// <summary>
        /// Instantiate the Slots</summary>
        public virtual void Awake ()
        {
            for(int i=0; i<_numberOfSlots; i++)
            {
                AddSlot();
            }
        }

        /// <summary>
        /// Reset all the slots.</summary>
        public void ResetSlots ()
        {
           foreach (Slot slot in _slots){
                Destroy(slot._item);
            }
            _slots.Clear();
            for (int i=0; i<_numberOfSlots; i++)
            {
                AddSlot();
            }
        }

        /// <summary>
        /// Add a Slot to the View.</summary>
        private Slot AddSlot ()
        {
            Slot slot = Instantiate(_slot);
            _slots.Add(slot);
            slot.transform.SetParent(_slotParent);
            slot.transform.localScale = new Vector3(1, 1, 1);
            slot.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, 0, 0);
            return slot;
        }

        /// <summary>
        /// Return the next available Slot or null if all Slots are taken.</summary>
        public virtual Slot NextAvailableSlot ()
        {
            foreach(Slot slot in _slots){
                if (slot._item == null)
                {
                    return slot;
                }
            }
            return _infiniteSlots ? AddSlot() : null;
        }

        /// <summary>
        /// Add an Item to the next available Slot.</summary>
        public virtual void AddItem (SlottableItem item)
        {
            Slot slot = NextAvailableSlot();
            if (slot != null)
            {
                slot.Drop(item);
            }
        }

        /// <summary>
        /// Clear the entire View by destroying all slot objects.</summary>
        public void ClearView ()
        {
            foreach(Slot slot in _slots)
            {
                if (!slot.IsFree)
                {
                    GameObject.Destroy(slot._item.gameObject);
                    slot.Clear();
                }
            }
        }

        /// <summary>
        /// Slots have changed.</summary>
        public virtual void SlotChanged (Slot slot,
                                         SlottableItem currentItem,
                                         SlottableItem previousItem)
        {
        }
    }
}
