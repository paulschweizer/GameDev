using UnityEngine;
using UnityEngine.EventSystems;

namespace SlotSystem
{
    /// <summary>
    /// Slot to hold a SlottableItem.</summary>
    public class Slot : MonoBehaviour, IDropHandler
    {
        /// <summary>
        /// The current Item in this slot</summary>
        public SlottableItem _item;

        /// <summary>
        /// An item is dropped onto this Slot.
        /// </summary>
        public void OnDrop(PointerEventData eventData)
        {
            Swap(SlottableItem._draggedItem);
        }

        /// <summary>
        /// Weather the Slot accepts the given Item.</summary>
        public virtual bool AcceptsItem (SlottableItem item)
        {
            return true;
        }

        /// <summary>
        /// Weather the Slot is free.</summary>
        public bool IsFree
        {
            get
            {
                return _item == null;
            }
        }

        /// <summary>
        /// Swap the given Item with the Item in this Slot if possible.</summary>
        public void Swap (SlottableItem item)
        {
            var currentItem = _item;
            var draggedItem = item;
            if (draggedItem == null || currentItem == draggedItem)
            {
                return;
            }
            var oldSlot = draggedItem._slot;

            if (IsFree && AcceptsItem(draggedItem) && draggedItem.AcceptsSlot(this))
            {
                Drop(draggedItem);
                oldSlot.Clear();
                return;
            }

            // Items need to be swapped
            if (AcceptsItem(draggedItem) && oldSlot.AcceptsItem(currentItem) &&
                draggedItem.AcceptsSlot(this) && currentItem.AcceptsSlot(oldSlot))
            {
                Drop(draggedItem);
                oldSlot.Drop(currentItem);
            }
        }

        /// <summary>
        /// Drop an Item onto this Slot.</summary>
        public void Drop (SlottableItem item)
        {
            var oldItem = _item;
            var newItem = item;
            item.transform.SetParent(transform);
            item.FitIntoSlot();
            ExecuteEvents.ExecuteHierarchy<ISlotChanged>(gameObject, null, (x, y) => x.SlotChanged(this, newItem, oldItem));
            _item = item;
            _item._slot = this;
        }

        /// <summary>
        /// Set the Slot free.</summary>
        public void Clear ()
        {
            var oldItem = _item;
            _item = null;
            ExecuteEvents.ExecuteHierarchy<ISlotChanged>(gameObject, null, (x, y) => x.SlotChanged(this, null, oldItem));
        }
    }

    /// <summary>
    /// Slots send a change signal when their Item has been changed.</summary>
    public interface ISlotChanged : IEventSystemHandler
    {
        void SlotChanged (Slot slot,
                          SlottableItem newItem,
                          SlottableItem oldItem);
    }
}
