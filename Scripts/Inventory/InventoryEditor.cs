#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

/// <summary>
/// Editor for Inventories.</summary>
[CustomEditor(typeof(Inventory))]
public class InventoryEditor : Editor
{
    /// <summary>
    /// Whether to show or hide the Item List</summary>
    private bool _showItems = true;

    /// <summary>
    /// Quantity of the Items to add</summary>
    private int _quantity = 1;

    /// <summary>
    /// The selected row holding the desired Item.</summary>
    int row;

    /// <summary>
    /// Draw all the UI elements.</summary>
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var inventory = (Inventory)target;

        var items = new List<InventoryItem>();
        var names = new List<string>();

        // Get all Items from the Directory
        EditorGUILayout.BeginHorizontal("box");
        var levelDirectoryPath = new DirectoryInfo(Application.dataPath + "/Content/Items");
        FileInfo[] assets = levelDirectoryPath.GetFiles("*.asset", SearchOption.AllDirectories);
        foreach (FileInfo asset in assets)
        {
            var item = (InventoryItem)AssetDatabase.LoadMainAssetAtPath("Assets/Content/Items/" + asset.Name);
            items.Add(item);
            names.Add(item.ToString());
        }
        row = EditorGUILayout.Popup(row, names.ToArray());
        _quantity = EditorGUILayout.IntField(_quantity);

        if(GUILayout.Button("+"))
        {
            inventory.AddItem(items[row], _quantity);
            EditorUtility.SetDirty(inventory);
        }
        EditorGUILayout.EndHorizontal();

        // Show all the items
        _showItems = EditorGUILayout.Foldout(_showItems, inventory._items.Count + " Items");
        if (_showItems)
        {
            DrawModelInEditor(inventory);
        }
    }

    /// <summary>
    /// Display the Model in the Editor.</summary>
    void DrawModelInEditor (Inventory inventory)
    {
        for (int i = 0; i < inventory._items.Count; i++)
        {
            EditorGUILayout.BeginHorizontal("box");
            string displayRow = inventory._items[i]._name + "\tx " + inventory._quantities[i];
            EditorGUILayout.LabelField(displayRow);

            if (GUILayout.Button("+"))
            {
                inventory.AddItem(inventory._items[i], 1);
                EditorUtility.SetDirty(inventory);
                EditorGUILayout.EndHorizontal();
                break;
            }
            if (GUILayout.Button("-"))
            {
                inventory.RemoveItem(inventory._items[i], 1);
                EditorUtility.SetDirty(inventory);
                EditorGUILayout.EndHorizontal();
                break;
            }
            if (GUILayout.Button("X"))
            {
                inventory.RemoveItem(inventory._items[i], inventory._quantities[i]);
                EditorUtility.SetDirty(inventory);
                EditorGUILayout.EndHorizontal();
                break;
            }
            EditorGUILayout.EndHorizontal();
        }

        if (GUILayout.Button("Reset"))
        {
            inventory.Reset();
            EditorUtility.SetDirty(inventory);
        }
    }
}
#endif
