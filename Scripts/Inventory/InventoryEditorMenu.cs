#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.IO;
using LitJson;
using System;
using System.Reflection;
using System.Collections.Generic;

/// <summary>
/// Editor Menu for the Inventory.</summary>
public class InventoryEditorMenu : EditorWindow
{
    // Generic
    int _type;
    string _name;
    int _price;
    string _description;

    // Weapon
    int _damage;
    int _range;
    int _speed;

    /// <summary>
    /// Dictionary to convert the Json Types to standard data types.</summary>
    public static Dictionary<string, Type> _jsonTypes = new Dictionary<string, Type>()
    {
        {"Int", typeof(Int32)},
        {"String", typeof(string)},
        {"Double", typeof(float)},
        {"Boolean", typeof(bool)}
    };

    [MenuItem ("Shadowhunter/New Item")]
    static void Init ()
    {
        // Get existing open window or if none, make a new one:
        var window = (InventoryEditorMenu)EditorWindow.GetWindow(typeof(InventoryEditorMenu));
        window.Show();
    }

    /// <summary>
    /// Shows the GUI.</summary>
    void OnGUI ()
    {
        // Type Selection Box
        _type = EditorGUILayout.Popup(_type, new []{"Generic", "Weapon"});
        _name = EditorGUILayout.TextField ("Name", _name);
       _price = EditorGUILayout.IntSlider("Price",_price, 0, 999);
        EditorGUILayout.LabelField("Description");
        _description = EditorGUILayout.TextField(_description, GUILayout.Height(50));

        // Weapon
        if (_type == 1)
        {
            _damage = EditorGUILayout.IntSlider("Damage", _damage, 0, 999);
            _range = EditorGUILayout.IntSlider("Range", _range, 0, 999);
            _speed = EditorGUILayout.IntSlider("Speed", _speed, 0, 999);
        }

        // Create the Item
        if (GUILayout.Button("Create Item", GUILayout.Height(30)))
        {
            CreateNewItem();
        }
    }

    /// <summary>
    /// Create a new Item as an Asset.</summary>
    void CreateNewItem ()
    {
        var levelDirectoryPath = new DirectoryInfo(Application.dataPath + "/Content/Items");
        FileInfo[] assets = levelDirectoryPath.GetFiles("*.asset", SearchOption.AllDirectories);

        if (_type == 0)
        {
            var item = ScriptableObject.CreateInstance<Generic>();
            item._type = "Generic";
            item._id = assets.Length;
            item._name = _name;
            item._price =_price;
            item._description = _description;
            AssetDatabase.CreateAsset(item, "Assets/Content/Items/" + item._name + ".asset");
            AssetDatabase.SaveAssets();
        }
        if (_type == 1)
        {
            var item = ScriptableObject.CreateInstance<Weapon>();
            item._type = "Weapon";
            item._id = assets.Length;
            item._name = _name;
            item._price =_price;
            item._description = _description;
            item._damage = _damage;
            item._range = _range;
            item._speed = _speed;
            AssetDatabase.CreateAsset(item, "Assets/Content/Items/" + item._name + ".asset");
            AssetDatabase.SaveAssets();
        }
    }

    /// <summary>
    /// Recreate the Item assets from the json files.</summary>
    [MenuItem("Shadowhunter/Rebuild Items")]
    private static void RebuildItemsFromDatabase()
    {
        // Get all Json Files
        var levelDirectoryPath = new DirectoryInfo(Application.dataPath + "/Content/Database");
        FileInfo[] jsonFiles = levelDirectoryPath.GetFiles("*.json", SearchOption.AllDirectories);

        foreach (FileInfo jsonFile in jsonFiles)
        {
            string content = File.ReadAllText(jsonFile.FullName);
            var jsonData = JsonMapper.ToObject(content);

            var columns = new List<string>();
            for (int c = 0; c < jsonData["columns"].Count; c++)
            {
                columns.Add(jsonData["columns"][c]["displayName"].ToString());
            }

            // Parse all the rows
            for (int r = 0; r < jsonData["rows"].Count; r++)
            {
                // Use the proper Item Type
                var item = InstantiateItemFromType(jsonData["rows"][r][1].ToString());

                // Assign all the data to the Item
                for (int i = 0; i < jsonData["rows"][r].Count; i++)
                {
                    object value = Convert.ChangeType(jsonData["rows"][r][i].ToString(),
                                                      _jsonTypes[jsonData["rows"][r][i].GetJsonType().ToString()]);
                    PropertyInfo propertyInfo = item.GetType().GetProperty(columns[i]);
                    propertyInfo.SetValue(item,
                                          Convert.ChangeType(value, propertyInfo.PropertyType),
                                          null);
                }
                AssetDatabase.CreateAsset(item, "Assets/Content/Items/" + item._name + ".asset");
                AssetDatabase.SaveAssets();
            }
        }
    }

    /// <summary>
    /// Create the proper Item based on the given type.
    /// @TODO Implement for all possible Items</summary>
    public static InventoryItem InstantiateItemFromType (string type)
    {
        InventoryItem item = null;
        if (type == "Generic")
        {
            item = ScriptableObject.CreateInstance<Generic>();
        }
        else if (type == "Weapon")
        {
            item = ScriptableObject.CreateInstance<Weapon>();
        }
        return item;
    }
}
#endif
