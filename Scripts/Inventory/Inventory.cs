using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The Inventory for Characters.</summary>
[Serializable]
public class Inventory : MonoBehaviour
{
    /// <summary>
    /// Maximum number of Slots</summary>
    [SerializeField]
    public int _numberOfSlots;

    /// <summary>
    /// The Items in the Inventory</summary>
    [SerializeField][HideInInspector]
    public List<InventoryItem> _items = new List<InventoryItem>();

    /// <summary>
    /// The quantities of the Items in the Inventory</summary>
    [SerializeField][HideInInspector]
    public List<int> _quantities = new List<int>();

    /// <summary>
    /// The Views associated with the Inventory</summary>
    public List<InventoryView> _views = new List<InventoryView>();

    /// <summary>
    /// Initialize the Lists if they are not yet initialized.</summary>
    void OnEnable ()
    {
        if (_items == null)
        {
            _items = new List<InventoryItem>();
        }
        if (_quantities == null)
        {
            _quantities = new List<int>();
        }
        if (_views == null)
        {
            _views = new List<InventoryView>();
        }
    }

    /// <summary>
    /// Initialize all views of this Inventory.</summary>
    void Start ()
    {
        foreach (InventoryView view in _views)
        {
            view.InitFromInventory(this);
        }
    }

    /// <summary>
    /// Associate the given view with this Inventory.</summary>
    public void AddView (InventoryView view)
    {
        _views.Add(view);
        view.InitFromInventory(this);
    }

    /// <summary>
    /// Add the given InventoryItem in the given quantity.</summary>
    public void AddItem (InventoryItem item, int quantity)
    {
        if (_items.Contains(item))
        {
            _quantities[_items.IndexOf(item)] += quantity;
            foreach (InventoryView view in _views)
            {
                view.ChangeItemData(item, _quantities[_items.IndexOf(item)]);
            }
        }
        else
        {
            _items.Add(item);
            _quantities.Add(quantity);
            foreach (InventoryView view in _views)
            {
                view.AddItem(item, quantity);
            }
        }
    }

    /// <summary>
    /// Remove the given InventoryItem in the given quantity.</summary>
    public void RemoveItem (InventoryItem item, int quantity)
    {
        _quantities[_items.IndexOf(item)] -= quantity;
        if (_quantities[_items.IndexOf(item)] < 1)
        {
            _quantities.RemoveAt(_items.IndexOf(item));
            _items.Remove(item);
            foreach (InventoryView view in _views)
            {
                view.RemoveItem(item);
            }
        }
        else
        {
            foreach (InventoryView view in _views)
            {
                view.ChangeItemData(item, _quantities[_items.IndexOf(item)]);
            }
        }
    }

    /// <summary>
    /// Clear the entire Inventory.</summary>
    public void Reset ()
    {
        _items.Clear();
        _quantities.Clear();
    }

    /// <summary>
    /// Transfer all the Items from the given Inventory.</summary>
    public void AddInventory (Inventory inventory)
    {
        for (int i=0; i < inventory._items.Count; i++)
        {
            AddItem(inventory._items[i], inventory._quantities[i]);
        }
        inventory.Reset();
    }
}
