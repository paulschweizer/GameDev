using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Mimics a database to search for Items.</summary>
public class ItemDatabase : MonoBehaviour
{
    public static ItemDatabase _itemDatabase;

    /// <summary>
    /// Static list holding all available Items.</summary>
    public List<InventoryItem> _items;

    protected void Awake ()
    {
        if (_itemDatabase == null)
        {
            DontDestroyOnLoad(gameObject);
            _itemDatabase = this;
        }
        else if(_itemDatabase != null)
        {
            Destroy(gameObject);
        }
    }

    public static List<InventoryItem> InventoryItem {
        get
        {
            return _itemDatabase._items;
        }
    }

    public static InventoryItem GetItemById (int id)
    {
        foreach (InventoryItem item in _itemDatabase._items)
        {
            if (item._id == id)
            {
                return item;
            }
        }
        return null;
    }
}
