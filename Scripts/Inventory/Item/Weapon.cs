using System;
using UnityEngine;

/// <summary>
/// Represents a Weapon in the Game.</summary>
[Serializable]
public class Weapon : InventoryItem
{
    /// <summary>
    /// The damage modifier for the total damage.</summary>
    [SerializeField] public float _damage;

    /// <summary>
    /// The range modifier for the total attack range.</summary>
    [SerializeField] public float _range;

    /// <summary>
    /// The speed modifier for the total speed.</summary>
    [SerializeField] public float _speed;

    /// <summary>
    /// Return a string representation of the stored data.</summary>
    public override string ToString()
    {
        return _id + "|" + _type + "|" + _name;
    }
}
