using UnityEngine;

/// <summary>
/// The property names correspond to the column names in the database.</summary>
public class InventoryItem : ScriptableObject
{
    /// <summary>
    /// Unique Id.</summary>
    [SerializeField] public int _id;

    /// <summary>
    /// The type corresponds to the name of the class.</summary>
    [SerializeField] public string _type;

    /// <summary>
    /// The name of the Item.</summary>
    [SerializeField] public string _name;

    /// <summary>
    /// Price value of the Item.</summary>
    [SerializeField] public int _price;

    /// <summary>
    /// Description of the Item.</summary>
    [SerializeField] [Multiline] public string _description;

    /// <summary>
    /// Return a string representation of the stored data.</summary>
    public override string ToString()
    {
        return _id + "|" + _type + "|" + _name;
    }
}
