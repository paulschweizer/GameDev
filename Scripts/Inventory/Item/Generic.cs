using System;
using UnityEngine;

/// <summary>
/// Same as the InventoryItem, but needed so the ItemDatabase can use
/// reflection for instantiating Generic Items.</summary>
[Serializable]
public class Generic : InventoryItem
{}
