using UnityEngine;
using System.Collections;

//------------------------------------------------------------------------------
// class definition
//------------------------------------------------------------------------------
public class MainMenuController : MonoBehaviour
{
    //--------------------------------------------------------------------------
    // Save / Load / Start Game
    //--------------------------------------------------------------------------

    /// <summary>
    /// @TODO documentation for Start</summary>
    public void New ()
    {
        MainController.New();
    }

    /// <summary>
    /// Continue from the last save state.</summary>
    public void Continue ()
    {
        // @TODO Check if there is a save state, if not, start a new Game
        MainController.Continue();
    }

    /// <summary>
    /// Exit the Game.</summary>
    public void Exit ()
    {
        MainController.Exit();
    }
}
