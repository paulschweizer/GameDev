using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// - Load and Unload Scenes
/// - Allocate and de-allocate Memory
/// - Save and Load the Game
/// - Start the Game
/// </summary>
public class MainController : MonoBehaviour
{
    // Necessary key prefabs for the game
    public PlayerController _playerControllerPrefab;
    public GameObject _eventSystemPrefab;
    public CameraRig _cameraRigPrefab;

    // Fade out
    public Canvas _fadingCanvas;
    public Image _fadingScreen;
    public float _fadingSpeed = 3;

    // From the Tutorial
    public static MainController _mainController;
    public string _currentSceneName;
    public string _nextSceneName;
    private AsyncOperation resourceUnloadTask;
    private AsyncOperation sceneLoadTask;
    private enum SceneState {FadeOut, Reset, Preload, Load, Unload, Postload, Ready, Run, FadeIn, Count};
    private SceneState sceneState;
    private delegate void UpdateDelegate();
    private UpdateDelegate[] updateDelegates;


    //--------------------------------------------------------------------------
    // public static methods
    //--------------------------------------------------------------------------

    public static void SwitchScene (string nextSceneName)
    {
        if (_mainController != null)
        {
            if (_mainController._currentSceneName != nextSceneName)
            {
                _mainController._nextSceneName = nextSceneName;
            }
        }
    }

    //--------------------------------------------------------------------------
    // Mono methods
    //--------------------------------------------------------------------------

    protected void Awake ()
    {
        if (_mainController == null)
        {
            DontDestroyOnLoad(gameObject);
            _mainController = this;
        }
        else if(_mainController != null)
        {
            Destroy(gameObject);
        }

        updateDelegates = new UpdateDelegate[(int)SceneState.Count];
        updateDelegates[(int)SceneState.FadeOut] = UpdateSceneFadeOut;
        updateDelegates[(int)SceneState.Reset] = UpdateSceneReset;
        updateDelegates[(int)SceneState.Preload] = UpdateScenePreload;
        updateDelegates[(int)SceneState.Load] = UpdateSceneLoad;
        updateDelegates[(int)SceneState.Unload] = UpdateSceneUnload;
        updateDelegates[(int)SceneState.Postload] = UpdateScenePostload;
        updateDelegates[(int)SceneState.Ready] = UpdateSceneReady;
        updateDelegates[(int)SceneState.FadeIn] = UpdateSceneFadeIn;
        updateDelegates[(int)SceneState.Run] = UpdateSceneRun;

        sceneState = SceneState.FadeIn;
    }

    /// <summary>
    /// Delete the delegates and the mainController statics.</summary>
    protected void OnDestroy ()
    {
        if (updateDelegates != null)
        {
            for (int i=0; i < (int)SceneState.Count; i++)
            {
                updateDelegates[i] = null;
            }
            updateDelegates = null;
        }
        // if (_mainController != null)
        // {
        //     _mainController = null;
        // }
    }

    protected void Update ()
    {
        if (updateDelegates[(int)sceneState] != null)
        {
            updateDelegates[(int)sceneState]();
        }
    }

    //--------------------------------------------------------------------------
    // private methods
    //--------------------------------------------------------------------------

    /// <summary>
    /// Fade out the level.</summary>
    private void UpdateSceneFadeOut ()
    {
        if (_fadingScreen.color.a < 1)
        {
            _fadingCanvas.sortingOrder = 999;
            _fadingCanvas.enabled = true;
            _fadingScreen.color = new Color(0f, 0f, 0f, _fadingScreen.color.a + Time.deltaTime*_fadingSpeed);
        }
        else
        {
           sceneState = SceneState.Reset;
        }
    }

    /// <summary>
    /// Attach the new scene controller to start cascade of loading.</summary>
    private void UpdateSceneReset ()
    {
        GC.Collect();
        sceneState = SceneState.Preload;
    }

    /// <summary>
    /// Handle anything that needs to happen before loading.</summary>
    private void UpdateScenePreload ()
    {
        sceneLoadTask = SceneManager.LoadSceneAsync(_nextSceneName);
        sceneState = SceneState.Load;
    }

    /// <summary>
    /// Show the loading screen until it's loaded.</summary>
    private void UpdateSceneLoad ()
    {
        if (sceneLoadTask.isDone)
        {
            sceneState = SceneState.Unload;
        }
        else
        {
            // Update some scene loading progress bar
        }
    }

    /// <summary>
    /// Clean up unused resources by unloading them.</summary>
    private void UpdateSceneUnload ()
    {
        if (resourceUnloadTask == null)
        {
            resourceUnloadTask = Resources.UnloadUnusedAssets();
        }
        else
        {
            if (resourceUnloadTask.isDone)
            {
                resourceUnloadTask = null;
                sceneState = SceneState.Postload;
            }
        }
    }

    /// <summary>
    /// Handle anything that needs to happen immediately after loading.</summary>
    private void UpdateScenePostload ()
    {
        _currentSceneName = _nextSceneName;
        sceneState = SceneState.Ready;
    }

    /// <summary>
    /// Handle anything that needs to happen immediately before running.
    /// 1. Instantiate the Player Character, and the Player Menu
    /// 2. Instantiate the Camera and connect it to the player character
    /// 3. Create an event system in the game scene if it is missing
    /// 4. Run the GameScene
    /// </summary>
    private void UpdateSceneReady ()
    {
        if (_nextSceneName != "Scenes/MenuScene")
        {
            // The Player
            Instantiate(_playerControllerPrefab);
            Instantiate(_playerControllerPrefab._playerPrefab);
            Instantiate(_playerControllerPrefab._playerMenuPrefab);

            // Make sure that Player and Menu both exist
            Player._player.transform.SetParent(PlayerController._playerController.transform);
            PlayerMenu._playerMenu.transform.SetParent(PlayerController._playerController.transform);

            // Connect the Player Inventory to the Player Menu
            var view = PlayerMenu._playerMenu.GetComponent<InventoryView>();
            Player._player._inventory.AddView(view);
            PlayerMenu._playerMenu.UpdateEntireMenu();

            // The CameraRig
            Instantiate(_cameraRigPrefab);
            CameraRig._cameraRig._target = Player._player.transform;
            CameraRig._cameraRig.transform.position = Player._player.transform.position;
            CameraRig._cameraRig.transform.rotation = Player._player.transform.rotation;

            // The EventSystem
            var eventSystem = FindObjectOfType<EventSystem>();
            if (eventSystem == null)
            {
                Instantiate(_eventSystemPrefab);
            }

            // Load the saved progress
            Load();
        }

        // The Game is now ready to run
        sceneState = SceneState.FadeIn;
    }

    /// <summary>
    /// Fade out the level.</summary>
    private void UpdateSceneFadeIn ()
    {
        if (_fadingScreen.color.a > 0)
        {
            _fadingScreen.color = new Color(0f, 0f, 0f, _fadingScreen.color.a - Time.deltaTime*_fadingSpeed);
        }
        else
        {
           _fadingCanvas.sortingOrder = 0;
           _fadingCanvas.enabled = false;
           sceneState = SceneState.Run;
        }
    }

    /// <summary>
    /// Wait for scene change.</summary>
    private void UpdateSceneRun ()
    {
        if (_currentSceneName != _nextSceneName)
        {
            sceneState = SceneState.FadeOut;
        }
    }

    //--------------------------------------------------------------------------
    // Save / Load / Start Game
    //--------------------------------------------------------------------------

    /// <summary>
    /// @TODO documentation for Start</summary>
    public static void New ()
    {
        // Switch to the Game Scene
        string dataFile = String.Format("{0}/Player.dat", GameStatesDirectory);
        if (File.Exists(dataFile))
        {
            File.Delete(dataFile);
        }
        MainController.SwitchScene("Scenes/GameScene");
    }

    /// <summary>
    /// Continue from the last save state.</summary>
    public static void Continue ()
    {
        // @TODO Check if there is a save state, if not, start a new Game
        MainController.SwitchScene("Scenes/GameScene");
    }

    /// <summary>
    /// Exit the Game.</summary>
    public static void Exit ()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #elif UNITY_WEBPLAYER
        Application.OpenURL(webplayerQuitURL);
        #else
        Application.Quit();
        #endif
    }

    /// <summary>
    /// Create a location for the Save Game.</summary>
    public static string GameStatesDirectory
    {
        get
        {
            var directory = String.Format("{0}/GameStates", Application.persistentDataPath);
            Directory.CreateDirectory(directory);
            return directory;
        }
    }

    /// <summary>
    /// - Player Stats
    /// - Inventory
    /// - Skills
    /// - Position
    /// - Scene
    /// - Enemies and Loot will have to be saved too at some point</summary>
    public static void Save ()
    {
        // Save the Player, including the Inventory
        // SaveData("Player", Player._player.Serialize());
    }

    /// <summary>
    /// Load the Game Progress from a saved location.</summary>
    public static void Load ()
    {
        // Load the Player, including the Inventory
        var serializedPlayer = (SerializedPlayer)LoadData("Player");
        if (serializedPlayer != null)
        {
            Player._player.DeSerialize(serializedPlayer);
        }
    }

    /// <summary>
    /// Save the given object under the given file name.</summary>
    public static void SaveData (string fileName, object data)
    {
        string dataFile = String.Format("{0}/{1}.dat", GameStatesDirectory, fileName);
        var file = new FileStream(dataFile,
                                  FileMode.OpenOrCreate,
                                  FileAccess.ReadWrite,
                                  FileShare.None);
        var bf = new BinaryFormatter();
        bf.Serialize(file, data);
        file.Close();
        Debug.Log("Saved: " + dataFile);
    }

    /// <summary>
    /// Load the given object stored at the given file name.</summary>
    public static object LoadData (string fileName)
    {
        string dataFile = String.Format("{0}/{1}.dat", GameStatesDirectory, fileName);
        if (File.Exists(dataFile))
        {
            var file = new FileStream(dataFile,
                                      FileMode.OpenOrCreate,
                                      FileAccess.ReadWrite,
                                      FileShare.None);
            var bf = new BinaryFormatter();
            var data = (object)bf.Deserialize(file);
            file.Close();
            Debug.Log("Loaded: " + dataFile);
            return data;
        }
        return null;
    }
}
