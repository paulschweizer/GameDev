using UnityEngine;

/// <summary>
/// Controller for the Player Character.</summary>
public class PlayerController : MonoBehaviour
{
    /// <summary>
    /// Static reference to the Player Controller.</summary>
    public static PlayerController _playerController;

    /// <summary>
    /// Prefab of the Player Character.</summary>
    public Player _playerPrefab;

    /// <summary>
    /// Controller for the Player Menu.</summary>
    public PlayerMenu _playerMenuPrefab;

    //--------------------------------------------------------------------------
    // protected mono methods
    //--------------------------------------------------------------------------

    /// <summary>
    /// Make sure the PlayerController is a Singleton.</summary>
    void Awake ()
    {
        if (_playerController == null)
        {
            _playerController = this;
        }
        else if(_playerController != null)
        {
            Destroy(gameObject);
        }
    }

    //--------------------------------------------------------------------------
    // actions
    //--------------------------------------------------------------------------

    /// <summary>
    /// Attack the closest Enemy.</summary>
    public static void PlayerAttackClosestEnemy ()
    {
        Player._player.AttackClosestEnemy();
    }

    //--------------------------------------------------------------------------
    // events
    //--------------------------------------------------------------------------

    /// <summary>
    /// Update the life Slider.</summary>
    public static void TakeDamage ()
    {
        PlayerMenu._playerMenu.UpdateLifeSlider();
    }

    /// <summary>
    /// What to do when the Player dies.</summary>
    public static void Death ()
    {
        MainController.SwitchScene("Scenes/MenuScene");
    }

    /// <summary>
    /// Update the Character and the Menu with the given experience.</summary>
    public static void GainExperience (int experience)
    {
        int prev_level = Player._player.Stats.Level;
        Player._player.Stats._experience += experience;
        int post_level = Player._player.Stats.Level;
        if (post_level > prev_level)
        {
            LevelUp();
        }
        PlayerMenu._playerMenu.UpdateExperienceSlider();
    }

    /// <summary>
    /// Update the Character and the Menu with the new level information.</summary>
    public static void LevelUp ()
    {
        Player._player.Stats._currentLife = Player._player.Stats.MaxLife;
        Player._player.Stats._availableAttributePoints += 5;
        Player._player.Stats._availableSkillPoints += 2;
        PlayerMenu._playerMenu.UpdateEntireMenu();
    }

    /// <summary>
    /// Player learned a Skill.</summary>
    public static void LearnSkill (Skill skill)
    {
        Player._player.Stats._availableSkillPoints -= skill._cost;
        Player._player.Stats._learnedSkills.Add(skill);
        Player._player.Stats.AddSkill(skill);
        Player._player.CharacterStatsChanged();
        PlayerMenu._playerMenu.UpdateEntireMenu();
    }

    /// <summary>
    /// Update all the Views associated with the Player.</summary>
    public static void UpdateAllViews ()
    {
        PlayerMenu._playerMenu.UpdateEntireMenu();
    }
}
