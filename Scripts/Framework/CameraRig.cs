using UnityEngine;

/// <summary>
/// Keep the Camera focused on the Player.</summary>
[ExecuteInEditMode]
public class CameraRig : MonoBehaviour
{
    /// <summary>
    /// Static reference, so the CameraRig is kept unique.</summary>
    public static CameraRig _cameraRig;

    /// <summary>
    /// The camera.</summary>
    public Camera _camera;

    /// <summary>
    /// The target for the camera to look at.</summary>
    public Transform _target;

    /// <summary>
    /// Minimal distance the camera keeps to the target.</summary>
    public float _minDist;

    /// <summary>
    /// Maximal distance the camera can be zoomed out.</summary>
    public float _maxDist;

    //--------------------------------------------------------------------------
    // mono methods
    //--------------------------------------------------------------------------

    /// <summary>
    /// Keep the CameraRig a singleton.</summary>
    void Awake ()
    {
        if (_cameraRig == null)
        {
            _cameraRig = this;
        }
        else if(_cameraRig != null)
        {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Aim at the target and process the mouse wheel zoom in and out.</summary>
    void Update ()
    {
        if (_target == null)
        {
            return;
        }
        transform.position = _target.position;

        // Zooming
        Vector2 scrollVect = Input.mouseScrollDelta;
        float scroll = scrollVect.y * (-1);
        if (scrollVect.magnitude != 0f) {
            float curDist = _camera.transform.position.y;
            if (curDist+scroll >= _minDist && curDist+scroll <= _maxDist) {
                var camPos = new Vector3(_camera.transform.position.x,
                                             _camera.transform.position.y + scroll,
                                             _camera.transform.position.z);
                _camera.transform.position = camPos;
            }
        }

        _camera.transform.LookAt(_target);
    }
}
