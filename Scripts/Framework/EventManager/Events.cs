using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Central place to handle all possible Player Actions.</summary>
public class Events : MonoBehaviour {

    /// <summary>
    /// Start listening.</summary>
    void OnEnable ()
    {
        EventManager.StartListening ("Clicked", ScreenClicked);
        EventManager.StartListening ("HoldDown", ScreenClicked);
    }

    /// <summary>
    /// Stop listening.</summary>
    void OnDisable ()
    {
        EventManager.StopListening ("Clicked", ScreenClicked);
        EventManager.StopListening ("HoldDown", ScreenClicked);
    }

    /// <summary>
    /// A click on the screen has occurred.
    /// Filters out GUI elements.
    /// The player input is being handled.</summary>
    void ScreenClicked ()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            Player._player._currentState.HandleInput();
        }
    }

    /// <summary>
    /// Pause the Game</summary>
    void Pause ()
    {
        Time.timeScale = 0;
        EventManager.StopListening ("Clicked", ScreenClicked);
        EventManager.StopListening ("HoldDown", ScreenClicked);
    }

    /// <summary>
    /// Resume the Game</summary>
    void Resume ()
    {
        Time.timeScale = 1;
        EventManager.StartListening ("Clicked", ScreenClicked);
        EventManager.StartListening ("HoldDown", ScreenClicked);
    }
}
