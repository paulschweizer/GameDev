using UnityEngine;
using System.Collections;

public class EventsTrigger : MonoBehaviour {

    void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            EventManager.TriggerEvent("Clicked");
        }

        if (Input.GetMouseButton(0))
        {
            EventManager.TriggerEvent("HoldDown");
        }
    }
}
