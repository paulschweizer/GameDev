# Fun and recreational Game Dev with Unity
The goal is to create a Game like the classic Diablo.
The focus lies on implementing the Game mechanics in a generic way.
In the first step, we'll not concern ourselves with graphics, animation, design etc.
Wherever possible, the Game mechanics are to be simplified as much as possible.

## Developer Information
For information on how to setup this project in Unity, please refer to the [DevREADME](/DevREADME.md)

Here's the Link to my personal [Developer Diary](https://docs.google.com/document/d/1Zha3vNIOJQcHk514E6sAagEYfw_dbcSiWWGFIG5I5XQ/edit?usp=sharing)

## Goals

- Player Character (Roleplaying Game style)
 - Player can configure Attributes like Strength, Health etc.
  - Attributes affect the secondary attributes like the Attack value, speed etc.
 - Levelling System
 - Skills (No real idea how to tackle this yet)
 - Equip Weapons and Armor
 - Use Items like Health Potions

- Enemies
 - Their Stats are configurable in the Unity Editor
 - Patrolling around the map
 - Very simple Fighting Algorithm, basically search for Player and Attack when in Range
 - Have an external definition sheet for enemies

- Effects on Characters
 - Be able to attach any number of Effects that alter the Player/Enemy attributes
 - Effects have a lifespan
 - Multiple Effects can be assigned

- Inventory System
 - MVC based System to hold and display Items in an in-game view
 - Inventories can be attached to any object (Player, Enemy, Treasure Chest)
 - All available Items are stored in a database of some sort (simple XML document at the moment)
 - Editable in the Unity Editor

- Level Maps
 - Simple, quadratic maps
 - No elevation changes
 - Using Unity's NavMeshAgent System
 - Avoid objects that would hide the Map

- Platforms
 - Browser
 - Mobile devices

- Main Menu
 - Pause
 - Resume
  - Resumes from the previously saved Game
 - Quit
  - Saves the current Game

- Camera View
 - Top down view centering the player character in the center of the screen
 - Zoom in out 
 - Create a ease-in/out movement for the camera movement instead of just staying fixed to the player 
